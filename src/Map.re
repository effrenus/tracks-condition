open Types;
open MapUtils;

type state = {
    loaded: bool,
    map: option(map)
};

type action =
  | NewMap(map);

let component = ReasonReact.reducerComponent("Map");

let setSectionRef = (theRef, {ReasonReact.state, ReasonReact.send}) => {
    if (!state.loaded) {
        switch (Js.Nullable.to_opt(theRef)) {
        | None => ()
        | Some(refVal) => {
            loadAPI() 
                |> Js.Promise.then_(
                    () => {
                        /* createMap(refVal); */
                        send(NewMap(createMap(refVal)));
                        Js.Promise.resolve()
                    }
            );
            ()
           }
        };
    }
};

let make = (~track, _children) => {
  ...component,
  initialState: () => { loaded: false, map: None },
  reducer: (action: action, state: state) =>
      switch (action) {
      | NewMap(map) => {
          Js.log("map");
          ReasonReact.Update({...state, loaded: true, map: Some(map)})
      }
      },
  render: (self) => {
    <div className="map" ref={self.handle(setSectionRef)}>
        (
            switch (track) {
            | None => ReasonReact.nullElement
            | Some(v) => <Polyline map=?self.state.map points=(v.coords) />
            }
        )
    </div>
  }
};