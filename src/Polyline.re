let component = ReasonReact.statelessComponent("Polyine");

let make = (~points, ~map, _children) => {
  ...component,
  didMount: (_) => {
    Js.log(points);
    let line = switch (map) {
    | Some(notnullMap) => Some(MapUtils.createPolyline(notnullMap, points))
    | None => None
    };
    ReasonReact.NoUpdate
  },
  render: (self) => {
    ReasonReact.nullElement
  }
};