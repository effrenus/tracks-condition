type api;
type map;
type polyline;

[@bs.module "./loadMapAPI"] external createMap : (Dom.element) => map = "createMap";

[@bs.module "./loadMapAPI"] external createPolyline : (map, array((float, float))) => polyline = "createPolyline";

[@bs.module "./loadMapAPI"] external loadAPI : unit => Js.Promise.t(unit) = "loadAPI";