type part = {
    from_: float,
    to_: float,
    description: string,
    title: string,
    condition: string
};

type track = {
    name: string,
    coords: array((float, float)),
    parts: array(part)
};

type tracksList = option(array(track));

type tracks = {
    tracks: tracksList
};