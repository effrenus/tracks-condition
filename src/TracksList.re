open Types;

let component = ReasonReact.statelessComponent("TracksList");

let renderList = (items, onClick) => 
    Array.mapi((index, item) => 
        <li onClick={(_) => onClick(index)}>(ReasonReact.stringToElement(item.name))</li>, items)
    |> ReasonReact.arrayToElement;

let make = (~tracks, ~onSelect, _children) => {
    ...component,
    render: (self) => {
        <ul>
            (ReasonReact.stringToElement("List:"))
            (
                switch (tracks) {
                | None => ReasonReact.nullElement
                | Some(trackList) => renderList(trackList, onSelect)
                }
            )
        </ul>
    }
}