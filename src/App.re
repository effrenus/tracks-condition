open Types;

type action =
  | Loaded (tracksList)
  | SelectTrack (int);

type state = {
    tracks: tracksList,
    selectedTrack: option(int),
    loading: bool
};

let component = ReasonReact.reducerComponent("App");

module Decoder = {
    let coord = json =>
        Json.Decode.( json |> pair(float, float) );
    
    let coordsArray = json =>
        Json.Decode.( json |> array(coord) );
    
    let partsArray = json =>
        Json.Decode.( json |> array(json => {
            from_: json |> field("from", float),
            to_: json |> field("to", float),
            title: json |> field("title", string),
            description: json |> field("description", string),
            condition: json |> field("condition", string)
        }) );

    let trackItem = json =>
        Json.Decode.{
            name: json |> field("name", string),
            coords: json |> field("coords", coordsArray),
            parts: json |> field("parts", partsArray)
        };

    let tracksArray = json =>
        Json.Decode.( json |> array(trackItem) );

    let tracksList = json =>
        Json.Decode.{
            tracks: json |> optional(field("list", tracksArray))
        };
};

let tracksArray = data =>
    data |> Json.parseOrRaise
         |> Decoder.tracksList;

let loadTracks = () =>
    Js.Promise.(
        Fetch.fetch("/tracks.json")
        |> then_(Fetch.Response.text)
        |> then_(json => {
            let decoded = tracksArray(json);
            resolve(decoded.tracks)
        })
    );

let getTrack = ({ selectedTrack, tracks }) => {
    let trackIndex = switch (selectedTrack) {
        | None => -1
        | Some(index) => index
        };
    switch (tracks) {
    | None => None
    | Some(elements) => trackIndex >= 0 ? Some(Array.get(elements, trackIndex)) : None
    }
};

let make = (_children) => {
    ...component,
    initialState: () => {
        tracks: None,
        selectedTrack: None,
        loading: true
    },
    didMount: (self) => {
        loadTracks()
            |> Js.Promise.then_(tracks => {
                self.send(Loaded(tracks));
                Js.Promise.resolve()
            }) 
            |> ignore;
        ReasonReact.NoUpdate
    },
    reducer: (action: action, state: state) =>
      switch (action) {
      | Loaded(t) => ReasonReact.Update({...state, loading: false, tracks: t})
      | SelectTrack(index) => ReasonReact.Update({...state, selectedTrack: Some(index)})
      },
    render: (self) => {
        <div>
            <Map track=(getTrack(self.state)) />
            <TracksList onSelect={(index) => self.send(SelectTrack(index))} tracks=(self.state.tracks) />
        </div>
    }
};