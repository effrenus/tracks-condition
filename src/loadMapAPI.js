function loadJs(url) {
    return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;

        const beforeScript = document.getElementsByTagName('script')[0];
        beforeScript.parentNode.insertBefore(script, beforeScript);
    });
}

const API_CALLBACK_NAME = '__goog__init_map';
const API_URL = `https://maps.googleapis.com/maps/api/js?key=AIzaSyBOxhESvL1x3_6SqBRfLitBUrReHbbaB2I&callback=${API_CALLBACK_NAME}`;
const API_STATUS = {
  NOT_INITED: 1,
  LOADING: 2,
  LOADED: 3,
};

let status = API_STATUS.NOT_INITED;
let loadPromise;

module.exports.createMap = function (elm) {
  return new google.maps.Map(elm, { center: {lat: 55.765505, lng: 37.609583}, zoom: 10 });
};

module.exports.createPolyline = function (map, points) {
  console.log(map, points)
  const line = new google.maps.Polyline({
    path: points.map(p => ({lat: p[0], lng: p[1]})),
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });
  line.setMap(map);
  return line;
};

module.exports.loadAPI =  async function loadAPI() {
  if (status !== API_STATUS.NOT_INITED) return loadPromise;

  status = API_STATUS.LOADING;

  loadPromise = new Promise((resolve, reject) => {
    window[API_CALLBACK_NAME] = () => {
      status = API_STATUS.LOADED;
      window[API_CALLBACK_NAME] = null;
      resolve();
    };

    loadJs(API_URL).catch(reject);
  });

  return loadPromise;
}